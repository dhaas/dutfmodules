/*
 * Terraform module for IAM roles
 *
 * Created By:   Duane Haas
 * Created Date: 9/5/2018
 *
 * Change Date  Change Owner    Change Description
 * -----------  --------------  --------------------------------------------------------------------------------------
 * 09/11/2018   Bob Peroutka    Add variable create_role to make aws_iam_role creation optional.
 */

variable "role_name" {
    description = "The name of the role."
}

variable "description" {
    description = "The description of the role."
    default = ""
}

variable "path" {
    description = "The path to the role.  Default is '/'"
    default = "/"
}

variable "assume_role_policy_name" {
    description = "The assume role policy json file name (e.g. iam_assume_role_aws_glue, iam_assume_role_s3, etc.)."
    default = ""
}

variable "role_policy_name" {
    description = "List of role policy names."
    default = ""
}

variable "role_policy" {
    description = "List of policy documents as a JSON formatted string."
    default = ""
}

variable "managed_policy" {
    description = "List of policy ARNs to apply."
    type = "list"
    default = []
}

variable "instance_profile_name" {
    description = "The instance profile name."
    default = ""
}

variable "create_role" {
    description = "Boolean to indicate role."
    default = true
}

data "template_file" "assume_role_policy" {
    count    = "${var.create_role}"

    template = "${file("${path.module}/${var.assume_role_policy_name}.json")}"
}

resource "aws_iam_role" "role" {
    count               = "${var.create_role}"

    name                = "${var.role_name}"
    assume_role_policy  = "${data.template_file.assume_role_policy.rendered}"
    path                = "${var.path}"
    description         = "${var.description}"
}

resource "aws_iam_role_policy" "role_policy" {
    count       = "${var.role_policy_name == "" ? 0 : 1}"

    role        = "${var.role_name}"
    name        = "${var.role_policy_name}"
    policy      = "${var.role_policy}"

    depends_on  = ["aws_iam_role.role"]
}

resource "aws_iam_role_policy_attachment" "attach_policy" {
    count       = "${length(var.managed_policy)}"

    role        = "${var.role_name}"
    policy_arn  = "${var.managed_policy[count.index]}"

    depends_on  = ["aws_iam_role.role"]
}

resource "aws_iam_instance_profile" "instance_profile" {
    count = "${var.instance_profile_name == "" ? 0 : 1}"

    name  = "${var.instance_profile_name}"
    role  = "${aws_iam_role.role.id}"
}

output "role_id" {
    value = "${join(",",aws_iam_role.role.*.id)}"
}

output "role_name" {
    value = "${join(",",aws_iam_role.role.*.name)}"
}

output "role_arn" {
    value = "${join(",",aws_iam_role.role.*.arn)}"
}

output "role_policy_ids" {
    value = "${join(",",aws_iam_role_policy.role_policy.*.id)}"
}

output "role_policy_name" {
    value = "${aws_iam_role_policy.role_policy.*.name}"
}
